class Cell(object):
	"""docstring for Cell"""
	MINE = 9
	EMPTY = 0

	def __init__(self, covered=True, flagged=False):
		self.value = self.EMPTY
		self.covered = covered
		self.flagged = flagged

	def __str__(self):
		if self.covered:
			if self.flagged:
				return 'F'
			return '#'
		if self.value == self.MINE:
			return '*'
		if self.value == self.EMPTY:
			return ' '
		return str(self.value)

	def putMine(self):
		self.value = self.MINE

	def flagCell(self, haveFlages=True):
		if haveFlages or self.flagged:
			self.flagged = not self.flagged
			return self.flagged*2-1
		return 0

	def uncoverCell(self):
		self.covered = False

	def increaseValue(self):
		self.value += 1

	def setValue(self, value):
		self.value = value

	def getValue(self):
		return self.value

	def isMine(self):
		return True if self.value == self.MINE else False

	def isEmpty(self):
		return True if self.value == self.EMPTY else False

	def haveNumber(self):
		return True if self.value > 0 and self.value < 9 else False

	def isCovered(self):
		return self.covered

	def isFlagged(self):
		return self.flagged

	def mineFlagged(self):
		return True if self.isMine() and self.covered and  self.flagged else False
		