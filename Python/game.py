import table
import os

def isInRange(message, max, min=0):
	while True:
	    value = int(input(message))

	    if value < min or value > max:
	        print('El valor ingresado no es valido favor de intentar otra vez')
	    else:
	    	return value

clear = lambda: os.system('cls')
clear()

class Game():
	def __init__(self, high=10, width=10, mines=10):
		self.tablero = table.Table(high, width, mines)

	def start(self):
		ruwining = True
		while ruwining:
			self.show()

			choise = isInRange('\n> Tap (1) o Bandera(2):',2,1)

			print('\n> Cordenadas del tap:')
			y = isInRange('> y: ', self.tablero.high)
			x = isInRange('> x: ', self.tablero.width)

			if choise == 1:
				mineClicked = self.tablero.clickCell(y,x)
				allCellsClicked = self.tablero.allCellsClicked()

				if mineClicked: 
					ruwining = False
					self.show('\n\nMINE CLICKED: YOU LOSE\n    GAME OVER')
				elif allCellsClicked:
					ruwining = False
					self.show('\n\nALL CELLS UNCOVERED: YOU WIN\n    GAME OVER')

			if choise == 2:
				self.tablero.flagCell(y,x)
				win4Flags = self.tablero.win4Flags()

				if win4Flags:
					ruwining = False					
					self.show('\n\nALL MINES FLAGGED: YOU WIN\n    GAME OVER')

	def show(self, message=''):
		clear()
		self.tablero.show(True)
		print(message)
