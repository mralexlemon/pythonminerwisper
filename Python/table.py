import random
import cell
import time

def isAdjacent(y, x, yv, xv):
	return True if yv>=y-1 and yv<=y+1 and xv>=x-1 and xv<=x+1 else False


class Table(object):
	"""docstring for Table"""
	ySlide = [-1,-1,-1,0,1,1,1,0]
	xSlide = [-1,0,1,1,1,0,-1,-1]

	def __init__(self, high=10, width=10, mines=10):
		self.table = []
		self.width = width
		self.high = high
		self.mines = mines
		self.minesSeted = False
		self.notMinedCells = width * high - mines
		self.cellClicked = 0
		self.cellFlagged = 0
		self.minesFlagged = 0
		for rowIndex in range(self.high):
			rowList = []
			for colIndex in range(self.width):
				rowList.append(cell.Cell(True))
			self.table.append(rowList)

	def putNumbers(self):
		for rowIndex in range(self.high):
			for colIndex in range(self.width):
				if not self.table[rowIndex][colIndex].isMine():
					totalMines = 0
					for i in range(8):
						yValue = rowIndex + self.ySlide[i]
						xValue = colIndex + self.xSlide[i]
						if self.validatePosition(yValue, xValue):
							if self.table[yValue][xValue].isMine():
								totalMines +=1
					if totalMines > 0:
						self.table[rowIndex][colIndex].setValue(totalMines)

	def validatePosition(self, yValue, xValue):
		if yValue < 0 or xValue < 0:
			return False
		if yValue >= self.high or xValue >= self.width:
			return False
		return True

	def flagCell(self, y, x):
		if self.validatePosition(y, x):
			if self.table[y][x].isCovered():
				sumValue = self.table[y][x].flagCell(self.haveFlags())
				self.cellFlagged += sumValue
				if self.table[y][x].isMine():
					self.minesFlagged += sumValue

	def clickCell(self, y, x, forceFlag=False, repeat=True):
		if self.validatePosition(y, x):
			if (self.table[y][x].isCovered() and 
					(not self.table[y][x].isFlagged() or forceFlag)):
				if not self.minesSeted:
					self.putMines(y, x)
					self.countMinesFlagged()
					self.minesSeted = True

				self.table[y][x].uncoverCell()
				self.cellClicked += 1

				if forceFlag and self.table[y][x].isFlagged():
					flagged = self.table[y][x].flagCell()
					self.cellFlagged += flagged

				if self.table[y][x].isEmpty():
					for i in range(8):
						yValue = y + self.ySlide[i]
						xValue = x + self.xSlide[i]
						self.clickCell(yValue, xValue, True, False)

				return True if self.table[y][x].isMine() else False
			elif self.table[y][x].haveNumber() and repeat:
				if self.countAdjacentFlags(y,x) == self.table[y][x].getValue():
					mineClicked = False
					for i in range(8):
						yValue = y + self.ySlide[i]
						xValue = x + self.xSlide[i]
						mineClicked = (self.clickCell(yValue, xValue, repeat=False) 
							or mineClicked)
					return mineClicked
		return False

	def countAdjacentFlags(self, y, x):
		count = 0
		for i in range(8):
			yValue = y + self.ySlide[i]
			xValue = x + self.xSlide[i]
			if self.validatePosition(yValue, xValue):
				if self.table[yValue][xValue].isFlagged():
					count += 1
		return count

	def putMines(self, yC, xC):
		for i in range(self.mines):
			do = True
			while do:
				y = random.randrange(self.high)
				x = random.randrange(self.width)
				#Si ya tiene mina o si esta dentro de la region inmediata 
				#busca otra posicion
				if not self.table[y][x].isMine() and not isAdjacent(y, x, yC, xC):
					do = False

			self.putMine(y, x)

	def putMine(self, y, x):
		if self.validatePosition(y, x):
			self.table[y][x].putMine()
			for i in range(8):
				yValue = y + self.ySlide[i]
				xValue = x + self.xSlide[i]
				self.increaseAdjacent(yValue, xValue)
				
	def increaseAdjacent(self, y, x):
		if self.validatePosition(y, x)
			if not self.table[y][x].isMine():
				self.table[y][x].increaseValue()

	def uncoverTable(self):
		for rowIndex in range(self.high):
			for colIndex in range(self.width):
				self.table[rowIndex][colIndex].uncoverCell()

	def show(self, showState=False):
		if showState:
			print('Uncovered: ' + str(self.cellClicked) + ', flagged: ' \
				+ str(self.cellFlagged) +'\n')
		row = '   '
		for i in range(self.width):
			row += str(i%10) + ' '
		print(row + '\n')

		for value, i in enumerate(self.table):
			row = str(value%10) + '  '
			for j in i:
				row += str(j) + ' '
			print(row)

	def numberCellsClicked(self):
		return self.cellClicked

	def allCellsClicked(self):
		return True if self.cellClicked == self.notMinedCells else False

	def win4Flags(self):
		return True if self.minesFlagged == self.mines else False

	def countMinesFlagged(self):
		self.minesFlagged = 0
		for rowIndex in range(self.high):
			for colIndex in range(self.width):
				if self.table[rowIndex][colIndex].mineFlagged():
					self.minesFlagged += 1

	def haveFlags(self):
		return True if self.cellFlagged < self.mines else False
