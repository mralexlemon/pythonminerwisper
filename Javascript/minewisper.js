class Cell {

	constructor(covered=true, flagged=false) {
		this.MINE = 9;
		this.EMPTY = 0;
		this.value = this.EMPTY;
		this.covered = covered;
		this.flagged = flagged;
	}

	show() {
		if(this.covered) {
			if(this.flagged) {
				return "F";
			}
			else {
				return "#";
			}
		}
		if(this.value == this.MINE) {
			return "*";
		}
		if(this.value == this.EMPTY) {
			return " ";
		}
		return this.value;
	}

	showState() {
		if(this.covered) {
			if(this.flagged) {
				return "Flag";
			}
			else {
				return "Cover";
			}
		}
		if(this.value == this.MINE) {
			return "Bomb";
		}
		if(this.value == this.EMPTY) {
			return "Blank";
		}
		return this.value;
	}

	putMine() {
		this.value = this.MINE;
	}

	flagCell(haveFlages=true) {
		if(haveFlages || this.flagged) {
			this.flagged = !this.flagged;
			return this.flagged * 2 - 1;
		}
		return 0;
	}

	uncoverCell() {
		this.covered = false;
	}

	increaseValue() {
		this.value += 1;
	}

	setValue(value) {
		this.value = value;
	}

	getValue() {
		return this.value;
	}

	isMine() {
		return this.value == this.MINE ? true : false;
	}

	isEmpty() {
		return this.value == this.EMPTY? true : false;
	}

	haveNumber() {
		return this.value > 0 && this.value < 9 ? true : false;
	}

	isCovered() {
		return this.covered;
	}

	isFlagged() {
		return this.flagged;
	}

	mineFlagged() {
		return this.isMine() && this.covered && this.flagged ? true : false
	}
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function isAdjacent(y, x, i, j) {
	return i>=y-1 && i<=y+1 && j>=x-1 && j<=x+1 ? true : false
}


function sendState(y, x, state) {
	console.log('y: ' + y + ', x: ' + x + ', state: ' + state)
}

class Table {
	constructor (high=10, width=10, mines=10) {
		this.ySlide = new Array(-1,-1,-1,0,1,1,1,0);
		this.xSlide = new Array(-1,0,1,1,1,0,-1,-1);
		this.width = width;
		this.high = high;
		this.mines = mines;
		this.notMinedCells = width * high - mines;
		this.minesSeted = false;
		this.cellClicked = 0;
		this.cellFlagged = 0;
		this.minesFlagged = 0;
		this.table = new Array();

		for(var i=0; i<high; i++) {
			var row = new Array();
			for(var j=0; j<width; j++) {
				var cellObject = new Cell();
				row.push(cellObject);
			}
			this.table.push(row);
		}
	}

	validatePosition(yValue, xValue) {
		return yValue<0 || xValue<0 || yValue >= this.high || xValue >= this.width ? false : true
	}

	putNumbers() {
		for(var y=0; y<this.high; y++) {
			for(var x=0; x<this.width; x++) {
				if(!this.table[y][x].isMine()) {
					var totalMines = 0
					for(var k=0; k<8; k++) {
						var i = y + this.ySlide[k];
						var j = x + this.xSlide[k];
						if(this.validatePosition(i, j)){
							if(this.table[i][j].isMine()){
								totalMines += 1;
							}
						}
					}
					if(totalMines > 0) {
						this.table[x][y].setValue(totalMines);
					}
				} 
			}
		}
	}

	flagCell(y, x) {
		if(this.validatePosition(y, x)) {
			if(this.table[y][x].isCovered()) {
				var sumValue = this.table[y][x].flagCell(this.haveFlags());
				sendState(y, x, this.table[y][x].showState());
				this.cellFlagged += sumValue;
				if(this.table[y][x].isMine()) {
					this.minesFlagged += sumValue;
				}
			}
		}
	}

	putMines(y, x) {
		for (var k=0; k<this.mines; k++) {
			var mineSeted = false;
			while(!mineSeted){
				var i = getRandomInt(0, this.high);
				var j = getRandomInt(0, this.width);
				if(!this.table[i][j].isMine() && !isAdjacent(y, x, i, j)) {
					mineSeted = true;
					this.putMine(i, j);
				}
			}
		}
	}

	putMine(y, x) {
		if(this.validatePosition(y, x)) {
			this.table[y][x].putMine();
			for (var k=0; k<8; k++) {
				var i = y + this.ySlide[k];
				var j = x + this.xSlide[k];
				this.increaseValue(i, j);
			}
		}
	}


	clickCell(y, x, forceFlag=false, repeat=true) {
		if(this.validatePosition(y,x)) {
			if(this.table[y][x].isCovered() && (!this.table[y][x].isFlagged() || forceFlag) ) {
				if(!this.minesSeted) {
					this.putMines(y,x);
					this.countMinesFlagged();
					this.minesSeted = true;
				}

				this.table[y][x].uncoverCell();
				sendState(y, x, this.table[y][x].showState());
				this.cellClicked += 1;

				if(forceFlag && this.table[y][x].isFlagged()) {
					var flagged = this.table[y][x].flagCell();
					this.cellFlagged += flagged;
				}
				if(this.table[y][x].isEmpty()) {
					for(var k=0; k<8; k++) {
						var i = y + this.ySlide[k];
						var j = x + this.xSlide[k];
						this.clickCell(i, j, true);
					}
				}
				return this.table[y][x].isMine() ? true : false;
			}
			if(this.table[y][x].haveNumber() && repeat) {
				if(this.countAdjacentFlags(y,x) == this.table[y][x].getValue()) {
					var mineClicked = false;
					for(var k=0; k<8; k++) {
						var i = y + this.ySlide[k];
						var j = x + this.xSlide[k];
						mineClicked = this.clickCell(i, j, false, false) || mineClicked;
					}
					return mineClicked;
				}
			}
		}
		return false;
	}

	countAdjacentFlags(y, x) {
		var count = 0;
		for(var k=0; k<8; k++) {
			var i = y + this.ySlide[k];
			var j = x + this.xSlide[k];
			if(this.validatePosition(i, j)) {
				if(this.table[i][j].isFlagged()) {
					count += 1;
				}
			}
		}
		return count;
	}

	increaseValue(y, x) {
		if(this.validatePosition(y, x)) {
			if(!this.table[y][x].isMine()) {
				this.table[y][x].increaseValue();
			}
		}
	}

	uncoverTable() {
		for(var y=0; y<this.high; y++) {
			for(var x=0; x<this.width; x++) {
				this.table[y][x].uncoverCell();
			}
		}
	}

	show(showState=false) {
		if(showState) {
			var message = 'Uncover: ' + this.cellClicked + ', Flagged: ' + this.cellFlagged;
			console.log(message);
		}

		var row = '   '
		for(var x=0; x<this.width; x++) {
			row += x%10 + ' ';
		}
		console.log(row);

		for(var y=0; y<this.high; y++) {
			var row = y%10 + '  '
			for(var x=0; x<this.width; x++) {
				row += this.table[y][x].show() + ' '
			}
			console.log(row)
		}
	}

	numberCellsCicked() {
		return this.cellClicked;
	}

	allCellsClicked() {
		return this.cellClicked == this.notMinedCells ? true : false;
	}

	win4Flags() {
		return this.minesFlagged == this.mines ? true : false;
	}

	countMinesFlagged() {
		this.minesFlagged = 0;
		for(var y=0; y<this.high; y++) {
			for(var x=0; x<this.width; x++) {
				if(this.table[y][x].mineFlagged()) {
					this.minesFlagged += 0;
				}
			}
		}
	}

	haveFlags() {
		return this.cellFlagged < this.mines ? true : false;
	}
}

class Game {
	constructor (mode='easy', high=10, width=10, mines=10) {
		switch(mode){
			case 'easy':
				this.tablero = new Table(8, 8, 10);
				break;
			case 'medium':
				this.tablero = new Table(16, 16, 40);
				break;
			case 'hard':
				this.tablero = new Table(16, 33, 99);
				break;
			default:
				this.tablero = new Table(high, width, mines);
				break;
		}
		this.showGame()
	}

	click(y, x) {
		var mineCliked = this.tablero.clickCell(y,x);
		var allCellsClicked = this.tablero.allCellsClicked();
		if(mineCliked) {
			this.showGame('Mina tocada: FIN DEL JUEGO');
		}
		else if(allCellsClicked) {
			this.showGame('Celdas descubiertas: FIN DEL JUEGO');
		}
		else {
			this.showGame();
		}
	}

	flag(y, x) {
		this.tablero.flagCell(y, x)
		var win4Flags = this.tablero.win4Flags()
		if(win4Flags) {
			this.showGame('Minas encontradas: FIN DEL JUEGO')
		}
		else {
			this.showGame()
		}
	}

	showGame(message='Hacer Click o Poner Bandera') {
		this.tablero.show(true)
		console.log(message)
	}
}